
\<Div style="text-align: center"\> \<p align="center"\> "A voice crying in the wilderness"\</p\> \</div\>  
  
\*As I stand upon above white swirling waves, I see dancing ripples of water express themselves as the sunlight reveals their beauty, s and into my mind flutters the memory of an innocent childhood filled with running and jumping and all the unique sounds of joy that bellow out of the mouths of children. Playing on a schoolyard—kickball, tetherball, hopscotch and all the experiences of the raw reality of a child's world fill my senses. They are the voices and visions of enchanted memories. They are the moments of . yesterday that are still alive in my heart and dominant my spirit..

\*Then my eyes open and I gaze into blurry shadows of men buzzing around, the sound of cell doors being racked and prison guards yelling “chow time!” Man, I just re-entered the beginning of a journey that held me captive for many years, and the things that once was fade away. Oh God, somebody shake me, somebody wake me! When will this nightmare is over?!

[I had a dream of playing major leag ue baseball, and ]()

[It feels as though it was only yesterday that my life was forfeited for a feeling.]()

[I had a dream of playing major leag ue baseball, and ]()
The prison yard became the unpeaceful workshops that produced every form of horror that one could imagine. If this was not so then we [would have]() all became a part of the pillars that gave birth to cultural death and the implementation of self-induced hatred. I lived inside the walls that had the highest restrictions on love that this age of human society has ever seen. From San Quentin to Folsom, and all the other human-molding sanctuaries, I became an institutionalized clone fulfilling all the specification that were designed to keep me locked away from my Kingdom Identity. Yeah, I was a real tough guy, a gangster, and all the things that went with it...I hid behind a curtain of self-created characters and personalities. [Now I know how easy it is to get caught up in who I felt I should be rather than to allow God’s Word to mold and shape me into the man I am becoming today.]() This journey lasted from 1964 to 1991. 
(By the way, if you are under the impression that you have never been to prison before, I would like for you to review your mental manuscripts and confront your own cell doors that may still be locked! Just a thought from me to you...)

* I take a deep breath and roll my head back. My left eye opens to a street sign that reads 5th and San Pedro. My eyes adjust to the blue skies, the tents and the people that are moving back and forth. My nostrils fill with the scent of human waste, and the air carries the sounds of lost dreams—lost hopes! I am surrounded by self-created destinies fueled by drugs and alcohol and any other thing that will numb the mind.
\* 

I became baptized into the flourishing culture of homelessness in downtown Los Angeles. It would become the essence of my enslavement and introduced me to the dehumanizing undercurrents of this reality. Death stalked me.

On any given day it was normal to cross paths with Count Dracula, the Wolfman and Frankenstein—the Halloween costume store was right around the corner. And the store that sold the pipe dreams and the solutions to our misery was always within reach, promising a new character in a new costume, yet producing the same lie again and again… A drowning man will reach for a blade of grass. However, I am surrounded by only muddy realities called heartbreak and despair.

It would finally culminate in the death of a very close friend. He died from an overdose of heroin. Like the cue ball that ricochets off the table, this was a poignant event that changed the course of my life. Only after months of grieving for my friend did I heard the spirit speak to me: “I will take one and save the other.” Acceptance. From that day forward a process began that derailed my ill-fated path and brought purpose to my destiny.

I traveled north.

In the Bay Area, I entered my first residential treatment program. A year later I graduated, but the long journey of recovery was just beginning. Following instructions has always been a problem for me, and my participation in recovery led me to think that the process would be like “the waving of a wand.” But I have since found that this journey of recovery is an Eternal Crossroad.

In 2014, I found myself again in the grips of a mental conspiracy to summon addiction in to my life, front and center and wish it well in all aspects of my self-proclaimed enslavement. I had surrendered 12 years of sobriety for a feeling, a feeling that had [lain]() dormant for many years, yet my mind again gave birth to it…

* I have wondered in the wilderness for four years with no solution to my misery, and not caring. I have embraced a daily injection of heroin into my arm.  And now, on this cold October morning, a relative very close to my heart just asked me to leave her house. It was more of a demand than a request. With a suitcase and a carry bag, I leave her apartment and make my way to a bus stop. “Yeah” I said it to myself, “why in the world would anybody want to put chains on me?” What a price I have paid for the illusion of independence.
\* 
[A delusional spirit controls the ambassadors of death.]()

While the day that I was booted from my relative’s home appeared to be another very sad day in my life, it has turned into a gateway. A door was cracked open that has given me the opportunity to walk into a freedom that surpasses all understanding. “Weeping endures for a night but joy comes in the morning.” [Psalms 30:5]() this would be the part of my journey that would seal up and heal the crevices of insecurity that had eaten away much of my soul. 

Part II

Residing at the Turning Point Crisis Center, where I had been housed for the past two and a half months. It was Christmas Day and I was graduating from the Crisis Center and the next day I would be going to the Grace House for an extended stay. As I look back on my arrival here it seems that it was just yesterday and already 15 months have passed. Unlike the 12 years I had clean in the Bay Area I have been given the opportunity to walk through some of the issues that plagued me for years. Here at the Grace House, I have had to come to grips with major challenges created by family and friends. My growth has been huge by way of making decisions; however, I have experienced a great deal of pain in my decision making. I have cried. I have sat night after night trying to solve heartbreaking equations, with no solution. I waded through turbulent storms of depression. Finally, one day my spirit cried out, “I cannot fix this!” And I hung my head with closed eyes, suffocating beneath guilt and shame. I sat there in silence. Then, in that terminal moment, God again spoke straight into my spirit. "Trust Me,” he said. And, at that moment, I actually did come to trust him. For once in my life, I surrendered and put my trust in the Lord. [That incident occurred eight months ago.]() I thank God for allowing the Light of Jesus to penetrate the desolate areas of my soul, declaring His Presence, His Glory and His Favor into all aspects of my life. In His Light, I waver not, but stay on the course that God has laid out before me. And I accept the fact that I must continue on this path for the duration of my corporeal life. 

 [As I entered into the third month at Grace House I was (we were) confronted with a plague of death that swept through the world causing devastation, despair and a sense of urgency that has left humanity on the doorsteps of uncertainty. [Its origin and birth are not foreign but rather a deadly contagious virus that is capable of wrapping its tentacles around and consuming the essence of life itself.]()]

 The covid, ...through the covid...

 [I have come to realize a man himself is the creator of all his sorrows--he allows the unknown to become the focal point of his purpose.]() Yes covid-19 for me has been like the death angel passing over my house and acknowledging the blood of the Lamb on the doorposts of my house, which would enable life to continue to praise God simply for who He is to us. 

Closing

In this year and three months since coming to Grace House I have witnessed a series of events that have had a powerful impact on life itself. There have been catastrophic global events, there have been individual gestures as well as collective gestures of human genocide, Yes I have seen the perils of the human condition alive and multiplying in the 21th century. Living in a time of [spiritual seduction]() and materialistic promises, unleashed are forces capable of destroying the very fabric of hope and the [universal rainbows]() that embrace the very substance of life. Amidst this calamity, I am learning my Kingdom Identity, learning how to accept my Identity and walk in the presence of God's Grace and Mercy. I am becoming a human expression of God's Word abiding in me, and me abiding in His Word. I am becoming the physical action of Faith, being the substance of all that we hope for and because of our belief, we are becoming the evidence of all that we could not see. I am walking amongst the echoes of God’s bellowing voice, I am wrapped in the Eternal Light that consumes all darkness, I am Vision, I am Discernment, I am the Voice of God's Word transmitting The Light that summons all people to draw near to His Presence………

“Draw near! Lean into my Presence, and know peace in your heart!”



